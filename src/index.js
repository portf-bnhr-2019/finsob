require('offline-plugin/runtime').install();

import './index.html';
import './login.html';
import './register.html';
import './job-list.html';
import './job-detail.html';
import './index.scss';
import './scripts/script.js';

import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';